import React, { Component } from 'react'
import { CartDetail, FoodList, FoodDetail, Header } from './component'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import CartContent from './component/CartContent';

export default class App extends Component {
  render() {
    return (
      <div>
        <Router>
          <CartDetail />
          <Switch>
            <Route exact path="/">
              <Header /><FoodList />
            </Route>
            <Route exact
              path="/detail/:id"
              render={
                (props) => <FoodDetail {...props} />
              }
            >
            </Route>
            <Route exact
              path="/content">
              <CartContent />
            </Route>
          </Switch>
        </Router>
      </div>
    )
  }
}
