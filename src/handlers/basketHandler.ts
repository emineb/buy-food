import { basketItemsSubject } from "../subjects/cartSubjects";

export const addToCart = (data: IFoodItem, event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    event.preventDefault();
    event.stopPropagation();

    basketItemsSubject.next({
        ...basketItemsSubject.value,
        list: getNewItems(data, [...basketItemsSubject.value.list]),
    })


    function getNewItems(newItem: IFoodItem, currentList: CartItemType[]) {

        var addedItem = currentList.find(c => c.item.id === newItem.id);

        if (addedItem)
            addedItem.quantity++;
        else {

            var newCartItem = {} as CartItemType;
            newCartItem.item = newItem;
            newCartItem.quantity = 1;
            currentList = [...currentList, newCartItem];
        }

        return currentList;
    }



};
export const removeFromCart = (data: CartItemType) => {

    basketItemsSubject.next({
        ...basketItemsSubject.value,
        list: [...basketItemsSubject.value.list.filter(c => c.item !== data.item)]

    })


};