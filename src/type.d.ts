interface IFoodItem {
    id: string;
    foodName: string;
    price: number;
    image: string;
    detail: string;
}

type FoodType = {
    foods: IFoodItem[];
    addToCart: (foodItem: IFoodItem) => void;
    removeFromCart: (id: string) => void;
}

type AddCartType = {
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>;
    foods: IFoodItem[];

}

type CartItemType = {
    item: IFoodItem;
    quantity: number;
}