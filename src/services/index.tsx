import axios from 'axios'
import { from, Observable } from 'rxjs';
const BASE_API = "https://602a50216c995100176ee29d.mockapi.io/food";


export const fetchFood = (): Observable<any> => {

    return from(axios.get(BASE_API))
};

export const fetchFoodById = (id: number): Observable<any> => {

    return from(axios.get(BASE_API + `/${id}`));
}

