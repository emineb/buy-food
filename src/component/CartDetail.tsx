import React, { Component } from 'react'
import "./CartDetail.scss"
import {
    Link
} from "react-router-dom";
import { basketItemsSubject, } from '../subjects/cartSubjects';
import { removeFromCart } from '../handlers/basketHandler';

export default class CartDetail extends Component {

    state = { cart: Array<CartItemType>(), loading: false }

    componentDidMount() {

        basketItemsSubject.subscribe({
            next: (data) => this.setState({ cart: data.list, loading: true }),
        })
    }

    emptyCart() {

        return (
            <div className="cart">
                <p>Empty Cart</p>
            </div>
        )
    }

    FullCart(carts) {

        return (
            <div className="cart">
                {carts.map(cartItem => (
                    <p className="cart-text" key={cartItem.item.id} >{cartItem.item.foodName} - {cartItem.quantity}
                        <button className="cart-remove" onClick={() => removeFromCart(cartItem)}>
                            X</button></p>))}
                <Link className="cart-link" to="/content">Go To Cart</Link>
            </div>

        )
    }

    render() {

        const carts = this.state.cart;
        return (
            <div className="cartDetail">
                <Link to="/" className="cartDetail-text">
                    Explore Our Special Menu
                </Link>
                <div className="cartDetail-dropdown">
                    <button className="cartDetail-dropdown-button">CART({carts.length > 0 ? carts.length : "0"})
                         </button>
                    <div className="cartDetail-dropdown-content">
                        {carts.length > 0 ? this.FullCart(carts) : this.emptyCart()}
                    </div>
                </div>
            </div>
        )
    }
}
