import React, { Component } from 'react'
import "./Header.scss"
export default class Header extends Component {

    render() {
        return (
            <div className="hero">
                <div className="hero-image">
                    <h1 className="hero-image-text">Special Menu</h1>
                </div>
            </div>
        )
    }
}
