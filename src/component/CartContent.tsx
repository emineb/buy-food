import React, { Component } from 'react';
import { removeFromCart } from '../handlers/basketHandler';
import { basketItemsSubject } from '../subjects/cartSubjects';
import "./CartContent.scss"

class CartContent extends Component {

    state = { cart: Array<CartItemType>() }

    componentDidMount() {

        basketItemsSubject.subscribe({
            next: (data) => this.setState({ cart: data.list })
        })
    }

    sum(cartItems) {
        let cartSum: number = 0;
        cartItems.map(carts => (
            cartSum += carts.item.price * carts.quantity
        ))
        return cartSum;
    }

    emptyCart() {
        return (
            <div className="emptyContent" >Empty Cart</div>
        )
    }

    cartContent(cart, sum) {
        return (<div className="cartContent">
            <table className="cartContent-table" >
                <tr className="cartContent-table-title" >
                    <th>Food Name</th>
                    <th>Quantity</th>
                    <th>Total</th>
                </tr>{cart.map(carts => (
                    <tr className="cartContent-table-detail" >
                        <td>{carts.item.foodName}</td>
                        <td>{carts.quantity}</td>
                        <td>{carts.item.price * carts.quantity} $</td>
                        <td>
                            <button className="cartContent-table-button" onClick={() => removeFromCart(carts)}>X</button>
                        </td>
                    </tr>))}
            </table>
            <div className="cartContent-element" >
                <p className="cartContent-element-allTotal">All Total: {sum} $</p>
                <button className="cartContent-element-buy">Buy
                  <span className="foodListButton-add-icon"></span>
                    <span className="cartContent-element-buy-icon"></span>
                </button></div>
        </div>);
    }

    render() {

        const carts = this.state.cart;
        const sum = this.sum(carts)

        return (
            <div className="content">  { carts.length !== 0 ? this.cartContent(carts, sum) : this.emptyCart()}</div>
        );
    }
}

export default CartContent;
