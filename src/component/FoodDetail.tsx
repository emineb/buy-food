import React, { Component } from 'react'
import "./FoodDetail.scss"
import { fetchFoodById } from '../services';
import { map } from 'rxjs/operators';
import { addToCart } from '../handlers/basketHandler';


export default class FoodDetail extends Component<any | IFoodItem> {
    state = { foodItem: {} as IFoodItem, loading: true };

    async componentDidMount() {
        const { id } = this.props.match.params;

        if (id) {

            const response = fetchFoodById(id)
            response.pipe(
                map((res) => res.data)
            ).subscribe({
                next: (data) => {
                    this.setState({ foodItem: data })
                },
                error: (error: any) => console.log(error),
                complete: () => this.setState({ loading: false })
            })
        }
    }

    detail(foodItem) {
        return (
            <div className="foodDetailItem" >
                <div className="foodDetailItem-text" key={foodItem.id}>
                    <h1 className="foodDetailItem-text-name" >{foodItem.foodName}</h1>
                    <p className="foodDetailItem-text-price">$ {foodItem.price}</p>
                    <p className="foodDetailItem-text-detail">{foodItem.detail}</p>
                    <button className="foodDetailItem-text-button" onClick={(event) => addToCart(foodItem, event)}>
                        Add To Cart
                   <span className="foodDetailItem-text-icon"> </span>
                    </button>
                </div>
                <img className="foodDetailItem-image" src={foodItem.image}
                    alt="foodDetailItemImage"
                    width="350" height="350" />
            </div>
        )
    }

    detailError() {
        return (
            <div className="foodDetail-loader">{ }</div>
        )
    }

    render() {
        const foodItem = this.state.foodItem;
        const loading = this.state.loading;

        return (
            <div className="foodDetail">
                {loading === true ? this.detailError() : this.detail(foodItem)}
            </div>
        )
    }
}
