import React, { Component } from 'react'
import "./FoodList.scss"
import {
    Link
} from "react-router-dom";
import { fetchFood } from "../services"
import { map } from 'rxjs/operators';
import { addToCart } from '../handlers/basketHandler';

export default class FoodList extends Component {

    state = { foods: Array<IFoodItem>(), loading: true };

    async componentDidMount() {

        const response = fetchFood()
        response.pipe(
            map((res) => res.data)
        ).subscribe({
            next: (data) => {
                this.setState({ foods: data })
            },
            error: (error: any) => console.log(error),
            complete: () => this.setState({ loading: false })
        })
    }


    listing(foods) {
        return (
            <div className="foodList-container">
                {foods.map((a) => (
                    <Link className="foodList-item" to={`/detail/${a.id}`} key={a.id}>
                        <div className="foodList-itemText">
                            <h1 className="foodList-itemText-name" >{a.foodName}</h1>
                            <p className="foodList-itemText-price">$ {a.price}</p>
                            <div className="foodListButton" >
                                <button className="foodListButton-add" onClick={(event) => (addToCart(a, event))}>Add To Cart
                                        <span className="foodListButton-add-icon"></span>
                                </button>
                            </div>
                        </div>
                        <img src={a.image} className="foodList-item-img" alt="foodList-img" width="100" height="100" />
                    </Link>
                ))}</div>
        )
    }

    listingError() {
        return (
            <div className="foodList-loader"></div>
        )
    }
    render() {

        const loading = this.state.loading;
        const foods = this.state.foods;

        return (
            <div className="foodList">{loading === true ? this.listingError() : this.listing(foods)}</div>
        )
    }
}
